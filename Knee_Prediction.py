import os
import time
import cv2
import numpy as np
import tensorflow as tf
from keras.models import load_model
import Load_Images

# Cargar modelo
model = load_model("modelo_final.h5")

# Ruta de salida para este archivo
ruta_imagenes = "Test\\IMGtest"

#Función que genera la predicción
def predecir(ruta_output):
    # Lista para almacenar las imágenes
    imagenes = []

    # Obtener la lista de archivos en la carpeta de salida
    archivos = os.listdir(ruta_output)

    # Leer cada imagen, convertirla a escala de grises y normalizar
    for i in archivos:
        imagen_path = os.path.join(ruta_output, f"{i}")
        imagen = Load_Images.preprocess_image(cv2.imread(imagen_path), (128, 128))
        imagenes.append(imagen)

    # Convertir a formato numpy y agregar dimensión del canal
    imagenes_np = np.array(imagenes)
    imagenes_np = np.expand_dims(imagenes_np, axis=-1)

    # Realizar la predicción
    predicciones = model.predict(np.array([imagenes_np]))

    # La salida será un valor entre 0 y 1
    return predicciones[0][0]

# Iterar sobre las imágenes en la carpeta de prueba
for archivo in os.listdir(ruta_imagenes):
    ruta_imagen = os.path.join(ruta_imagenes, archivo)

    # Imprimir el nombre del archivo y la predicción del modelo
    print (archivo, predecir(ruta_imagen))

