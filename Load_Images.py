import os
import cv2
import pandas as pd
import numpy as np
from keras.preprocessing import image

def preprocess_image(image, image_size):
    # Convierte a escala de grises
    img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # Normaliza y redimensiona la imagen
    img_resized = cv2.resize(img_gray, image_size)
    img_resized = img_resized / 255.0
    return img_resized

def build_dataset(data, data_dir, image_size):
    images = []
    labels = []

    for index, row in data.iterrows():
        folder_name = str(f"{row.iloc[0]:04d}")
        label = row.iloc[1]

        folder_path = os.path.join(data_dir, folder_name)

        image_sequence = []

        for filename in os.listdir(folder_path):
            image_path = os.path.join(folder_path, filename)

            image = cv2.imread(image_path)
            # Aplica el preprocesamiento a cada imagen
            image = preprocess_image(image, image_size)
            image = np.expand_dims(image, axis=-1)  # Agregar dimensión del canal
            image_sequence.append(image)

        images.append(image_sequence)
        labels.append(label)

    return np.array(images), np.array(labels)

def load_images_from_folder(folder, image_size, label):
    images = []
    labels = []

    for case_folder in os.listdir(folder):
        folder_path = os.path.join(folder, case_folder)
        secuence_images = []
        
        for filename in os.listdir((folder_path)):
            image_path = os.path.join(folder_path, filename)

            image = cv2.imread(image_path)
            # Aplica el preprocesamiento a cada imagen
            image = preprocess_image(image, image_size)
            image = np.expand_dims(image, axis=-1)  # Agregar dimensión del canal
            secuence_images.append(image)

        images.append(secuence_images)
        labels.append(label)  # Carpeta 0 o 1
    return np.array(images), np.array(labels)
