# Documentación del código Edit_Images

## Librerías Importadas en el código

- `os`: Se utiliza para crear subcarpetas numeradas en una ubicación de salida específica para organizar y almacenar las imágenes generadas.
- `numpy`: Permite realizar operaciones numéricas y matemáticas con los datos de imagen.
- `PIL`: Se utiliza para trabajar con imágenes en formato JPEG, ofrece funciones para abrir, crear y manipular imágenes en diferentes formatos.
- `pydicom`: Se utiliza para trabajar con datos en formato DICOM (Digital Imaging and Communications in Medicine), que permite el almacenamiento y transmisión de imágenes médicas.

# Código 

Este código Python define funciones las cuales son útiles para procesar y manipular secuencias de imágenes médicas en formato Numpy, DICOM y JPEG.

## Función numpy_to_jpeg

La función toma una serie de imágenes médicas en formato numpy, realiza operaciones opcionales de recorte y extraccion, y guarda las imágenes resultantes en formato JPEG en una estructura de carpetas numeradas en la ubicación especificada.

### Parámetros

- `rutaInput`: Ruta del archivo de entrada que contiene las imágenes en formato NumPy.
- `rutaOutput`: Ruta del directorio de salida donde se guardarán las imágenes en formato JPEG.
- `num_imagenes_a_extraer`(opcional): Número de imágenes a extraer del conjunto total. Si no se proporciona, se convierten todas las imágenes disponibles.
- `porcentaje_de_recorte`(opcional): Porcentaje de recorte a aplicar a las imágenes. Si no se proporciona, las imágenes se guardan sin recorte.

### Funcionamiento

La función inicia su tarea al cargar un archivo numpy ubicado en la ruta especificada por `rutaInput`. Este archivo contiene una serie de imágenes médicas, como las obtenidas mediante resonancia magnética (MRI). Después se procede a crear un directorio de salida en la ubicación definida por `rutaOutput`.
Si se ha especificado la cantidad de imágenes, la función asegura que este número no exceda la cantidad total de imágenes disponibles en la serie. Luego, calcule el índice inicial para extraer las imágenes del centro de la serie.
La función inicia un bucle que itera sobre el rango definido por `inicio` y `inicio + num_imagenes_a_extraer`. En cada iteración, realiza las acciones:
- Creación de Ruta de Archivo JPEG
- Creación de Objeto de Imagen
- Aplicación de Recorte (Opcional)
- Guardado de Imagen en Formato JPEG

## Función zoom_image

La función realiza un recorte del centro de una imagen, especificando un porcentaje de recorte.

### Parámetros

- `imagen`: Imagen de entrada (objeto de la clase Image de PIL).
- `porcentaje_de_recorte`: Porcentaje a recortar del centro.

### Funcionamiento

La función inicia extrayendo las dimensiones de la imagen original, es decir, su ancho y alto.
Posteriormente, procede a calcular las nuevas dimensiones después de aplicar el recorte. La función reduce tanto el ancho como el alto de la imagen original en función del porcentaje de recorte proporcionado. 
Luego, se determinan las coordenadas de recorte necesarias para extraer la región centrada de la imagen. 
Toma estas coordenadas calculadas y recorta la región definida por ellas.
Finalmente, devuelve la imagen recortada como resultado de la operación.

## Función dicom_to_jpeg

Esta función toma archivos DICOM de RMN, los convierte en imágenes JPEG y las organiza en carpetas separadas según la serie a la que pertenecen.

### Parámetros

- `dicom_directory` (str): Ruta al directorio que contiene los archivos DICOM.
- `output_directory` (str): Ruta al directorio de salida para guardar las imágenes JPEG.

### Funcionamiento

Se recibe la ubicación del directorio que contiene los archivos DICOM ( `dicom_directory`) y la ubicación del directorio de salida donde se guardarán las imágenes JPEG organizadas (`output_directory`). Se crea un diccionario llamado `series_dict` para organizar las imágenes por serie o secuencia. Esto se utilizará para llevar un registro de las imágenes que pertenecen a cada serie.
Se escanea el directorio DICOM y sus subdirectorios utilizando `os.walk` para encontrar todos los archivos DICOM.
Para cada archivo encontrado, lo utilizando la biblioteca pydicom, lo que permite acceder a su contenido.
Se extrae el nombre de la secuencia y el número de imágen del archivo DICOM, a través de `ProtocolNamedel` y `InstanceNumber`.
Se verifica si existe `output_directory`. Si no existe, se crea una carpeta para esa serie.
La imagen DICOM se convierte a formato JPEG. La intensidad de los píxeles se ajusta para que la imagen tenga un rango de valores entre 0 y 255.
La imagen en formato JPEG se guarda en la carpeta de la serie con un nombre de archivo que incluye el número de imagen dentro de la serie.
Se actualiza el diccionario `series_dict` para llevar un registro de las imágenes en cada serie.

## Función edit_jpeg

La función realiza operaciones de edición en imágenes JPEG ubicadas en un directorio de entrada, aplicando un proceso de zoom y guardando las imágenes resultantes en un directorio de salida

### Parámetros

- `rutaInput`(str): Ruta del directorio que contiene las imágenes JPEG originales que se editarán.
- `rutaOutput`(str): Ruta del directorio donde se guardarán las imágenes JPEG editadas.
- `num_imagenes_a_extraer`(int, opcional): Número opcional de imágenes a extraer y editar. Si no se proporciona, la función procesará todas las imágenes en el directorio de entrada.
- `porcentaje_de_recorte`(float, opcional): Porcentaje opcional para recortar del centro de cada imagen durante el proceso de edición. Debe ser un valor decimal entre 0 y 1. Si no se proporciona, no se aplicará ningún recorte.

### Funcionamiento

La función utiliza `os.listdir` para obtener una lista de archivos en el directorio especificado por `rutaInput`. Luego, ordena esta lista numéricamente.
La función itera sobre cada archivo en la lista ordenada. Cada archivo representa una imagen JPEG en el directorio de entrada.
Calcula el índice inicial para determinar desde qué posición se extraerán las imágenes del centro. 
Dentro de un bucle for, itera sobre un rango definido.
Para cada imagen en el rango, realiza los siguientes pasos:
- Creación de Rutas de Archivo
- Carga de la Imagen Original
- Aplicación de Zoom a la Imagen
- Guardado de la Imagen con Zoom