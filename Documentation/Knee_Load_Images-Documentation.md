# Documentación del código Load_Images

## Librerías Importadas en el código

- `os`: Proporciona una forma de interactuar con el sistema operativo. Se utiliza para tareas como navegar por directorios, crear rutas y enumerar archivos.
- `cv2`: Se utiliza para tareas de procesamiento de imágenes y visión por computadora. Proporciona funciones para la manipulación de imágenes, incluida la lectura, conversiones de color y cambio de tamaño.
- `pandas`: Se utiliza para la manipulación y el análisis de datos. 
- `numpy`: Permite realizar operaciones numéricas y matemáticas en los datos de imagen, como la conversión de datos en imágenes.
- `keras.preprocessing import image`: Proporciona utilidades para cargar y preprocesar imágenes.

# Código 

Este código Python proporciona funciones para preprocesar imágenes médicas y construir conjuntos de datos a partir de directorios de imágenes. 

## Función preprocess_image

Esta función toma una imagen en formato OpenCV y la convierte en una escala de grises, la normaliza y redimensiona.

### Parámetros

- `image`(numpy.ndarray): Imagen de entrada en formato OpenCV (BGR).
- `image_size`(tupla): Tamaño deseado para la imagen de salida, especificado como (ancho, alto).

### Funcionamiento

La función toma una imagen en formato OpenCV y utiliza `cv2.cvtColor` para convertirla a escala de grises.
Luego, normaliza la imagen dividiendo cada píxel por 255.0 para asegurar que los valores estén en el rango [0, 1].
Redimensiona la imagen al tamaño especificado por `image_size` utilizando `cv2.resize`.
Finalmente, devuelve la imagen preprocesada y redimensionada.

## Función build_dataset

Esta función construye un conjunto de datos a partir de un DataFrame de pandas que contiene información sobre las imágenes y su directorio correspondiente.

### Parámetros

- `data`(pandas.DataFrame): DataFrame que contiene información sobre las imágenes y sus etiquetas.
- `data_dir`(str): Directorio principal que contiene carpetas con imágenes.
- `image_size`(tupla): Tamaño deseado para las imágenes de salida, especificado como (ancho, alto).

### Funcionamiento

La función inicializa las listas `images` y `labels` para almacenar las imágenes preprocesadas y sus etiquetas correspondientes.
Itera sobre cada fila del DataFrame `data` que contiene información sobre las imágenes y sus etiquetas.
Para cada fila, obtiene el nombre de la carpeta y la etiqueta.
Construya la ruta completa de la carpeta utilizando `os.path.join`.
Itera sobre los archivos en esta carpeta, carga cada imagen con OpenCV y aplica la función `preprocess_image`.
Almacena las secuencias de imágenes preprocesadas y la etiqueta correspondiente en las listas `images` y `labels`.
Finalmente, devuelve las listas convertidas en arrays numpy: (images, labels).

## Función load_images_from_folder

Esta función carga imágenes desde un directorio y las organiza en secuencias, cada secuencia representando un caso médico.

### Parámetros

- `folder`(str): Directorio que contiene carpetas con imágenes organizadas por casos médicos.
- `image_size`(tupla): Tamaño deseado para las imágenes de salida, especificado como (ancho, alto).
- `label`(int): Etiqueta asignada a las imágenes cargadas.

### Funcionamiento
La función inicializa listas `images` y `labels` para almacenar secuencias de imágenes preprocesadas y sus etiquetas.
Itera sobre las carpetas en el directorio especificado (`folder`), cada una representando un caso médico.
Para cada carpeta, construye la ruta completa.
Itera sobre los archivos en la carpeta, carga cada imagen con OpenCV y aplica la función `preprocess_image`.
Almacena las secuencias de imágenes preprocesadas y la etiqueta especificada en las listas `images` y `labels`.
Finalmente, devuelve las listas convertidas en arrays numpy: (images, labels).