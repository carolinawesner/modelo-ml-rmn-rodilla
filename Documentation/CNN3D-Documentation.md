# Documentación del código- Modelo CNN 3D para clasificación de imágenes de RMN de rodilla

Este código implementa una red neuronal convolucional tridimensional (CNN3D) utilizando Keras y TensorFlow para la clasificación binaria de imágenes de Resonancia Magnética Nuclear (RMN) de rodilla
en el plano sagital.

Categorías:
- 0: Normal
- 1: Lesion de LCA

## Librerías Importadas

- `os`: Proporciona funciones para interactuar con el sistema operativo, como la manipulación de rutas de archivos y directorios.
- `numpy`: Se utiliza para la manipulación de datos en forma de matrices, especialmente en la combinación de imágenes y etiquetas.
- `tensorflow`: Proporciona la base para la construcción y entrenamiento del modelo de CNN3D.
- `keras`: Se utiliza para definir y compilar modelos de redes neuronales, así como para especificar métricas y funciones de pérdida.
- `Sequential` de `keras.models`: Proporciona una forma lineal de construir modelos de Keras, donde las capas se añaden secuencialmente una después de otra.
- `Conv3D, MaxPooling3D, Flatten, Dense, Dropout` de `keras.layers`: Definen la arquitectura del modelo CNN3D.
- `train_test_split` de`sklearn.model_selection`: Se utiliza para dividir el conjunto de datos en conjuntos de entrenamiento y validación.
- `load_images_from_folder` de`Load_Images`: Módulo personalizado que se utiliza para cargar imágenes y etiquetas desde directorios específicos.

## Configuración de la GPU

Establece `CUDA_VISIBLE_DEVICES` para utilizar solo la CPU en lugar de la GPU.

## Parámetros:

- `data_dir_0`: Directorio que contiene las imágenes de la clase "Normal" 
- `data_dir_1`: Directorio que contiene las imágenes de la clase "Lesión de LCA".
- `image_size`: Tamaño de las imágenes. Se establece en (128, 128).

## Preparación de Datos

Se cargan las imágenes y etiquetas para las clases 0 y 1 utilizando la función `load_images_from_folder`.
Éstas se combinan y se dividen en conjuntos de entrenamiento y validación utilizando `train_test_split`.

## Modelo CNN 3D Personalizado

El modelo CNN 3D consta de varias `capas convolucionales (Conv3D)`, `capas de pooling(MaxPooling3D)`, `capas de aplanamiento (Flatten)`, `capas completamente conectadas(Dense)` y `capas de Dropout`.

- 2 Capas convolucionales y de activación.
- 2 Capas de pooling para reducir la dimensionalidad.
- 1 Capa de aplanamiento.
- 3 Capas completamente conectadas.
- 1 Capa de dropout del 0.1.
- 1 Capa de salida con activación sigmoid para clasificación binaria.

En esta etapa se toma 1 estrategia para poder compensar el sobreajuste:

- Capa de doupout: desactiva aleatoriamente un porcentaje de neuronas durante el entrenamiento, reduciendo así la interdependencia y la especialización excesiva de las neuronas.

En la primera Conv3D, se especifica la forma de entrada de los datos a través de `input_shape=(9, 128, 128, 1)`. Se espera una secuencia de 9 imágenes de 128px de alto, 128px de ancho y de 1 canal (ecala de grises).

## Compilación del Modelo

El modelo se compila utilizando la función de pérdida `binary_crossentropy`, para problemas de clasificación binaria. La misma mide la discrepancia entre las predicciones del modelo y las etiquetas reales.

Se elige como optimizador `Adam` con una tasa de aprendizaje de 1e-4. Un optimizador en el contexto del aprendizaje automático es un algoritmo que determina cómo los pesos del modelo deben ser ajustados para mejorar el aprendizaje. La tasa 1e-4, es la tasa de actualización de los pesos del modelo. Esta es una tasa menor a la habitual para mitigar aún más el sobreajuste. Además, se agregan las siguientes métricas de evaluación:

-  Se agrega la métrica de `BinaryAccuracy` para evaluar la precisión del modelo en términos de clasificación binaria.
- `Area under the ROC Curve (AUC)` para evaluar la capacidad de clasificación del modelo.

## Guardado del Mejor Modelo

El modelo se guarda en un archivo h5 utilizando la función `checkpoint`. Este método establece los parámetros de guardado, incluyendo la ubicación y tratamiento de capas. Además, selecciona el mejor modelo comparando la metrica `binary_accuracy`, quedandose siempre con la de mayor rendimiento.

## Entrenamiento del modelo

El modelo se entrena con los conjuntos de entrenamiento y validación durante 30 épocas, con un tamaño de lote de 32.

## Resumen del modelo:

Se muestra un resumen detallado del modelo que incluye información sobre cada capa, el número de parámetros y la estructura general de la red.

---

**NOTA**: Este documento proporciona una descripción general de la estructura y funcionamiento del código. Para obtener detalles más específicos sobre cada parte del código, se recomienda revisar los comentarios incluidos en el mismo.