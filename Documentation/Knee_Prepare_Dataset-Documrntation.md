# Documentación del código Prepare_Dataset

## Librerías Importadas en el código

- `os`: Proporciona una forma de interactuar con el sistema operativo. Se utiliza para tareas como navegar por directorios, crear rutas y enumerar archivos.
- `Edit_Images`: Se utiliza para procesar y manipular secuencias de imágenes médicas en formato Numpy, DICOM y JPEG.

# Código 

Este código organiza y procesa imágenes médicas en formato Numpy y JPEG, creando directorios y aplicando conversiones específicas para cada formato.

### Conversión de Archivos Numpy a JPEG

Este bloque de código se encarga de convertir imágenes en formato Numpy a formato JPEG. 

- Definición de Rutas: Se especifica la ruta del directorio que contiene las imágenes en formato Numpy y la ruta base donde se crearán las carpetas con los casos y las imágenes en formato JPEG.
- Obtención de Archivos Numpy: Se utiliza `os.listdir` para obtener la lista de archivos en `archivos_numpy`.
- Iteración sobre Archivos Numpy: Se inicia un bucle `for` para iterar sobre cada archivo en la lista de archivos Numpy.
- Extracción del Nombre del Archivo: Para cada archivo, extrae el nombre sin extensión utilizando `os.path.splitext`.
- Creación de Ruta de Salida: Se construye la ruta de salida para este archivo concatenando la ruta base con el nombre del archivo.
- Carga y Procesamiento de la Imagen: Se construye la ruta completa de la imagen Numpy utilizando `os.path.join` y se llama a la función `Edit_Images.numpy_to_jpeg` para convertir y procesar la imagen.

### Conversión de archivos JPEG

Este bloque de código se encarga de convertir imágenes en formato JPEG, además de organizarlas en directorios

- Definición de Rutas: Se especifica la ruta del directorio que contiene las imágenes en formato JPEG.
- Obtención de Archivos JPEG: Se utiliza `os.listdir`para obtener la lista de `archivos_jpeg`.
- Iteración sobre Archivos JPEG: Se inicia un bucle `for` para iterar sobre cada archivo en la lista de archivos JPEG.
- Extracción del Nombre del Archivo: Para cada archivo, extrae el nombre sin extensión utilizando `os.path.splitext`.
- Creación de Ruta de Salida: Se construye la ruta de salida para este archivo concatenando la ruta base con el nombre del archivo convertido a entero.
- Carga y Procesamiento de la Imagen: Se construye la ruta completa de la imagen JPEG utilizando `os.path.join` y se llama a la función `Edit_Images.edit_jpeg` para procesar la imagen.