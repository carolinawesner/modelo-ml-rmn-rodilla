import os
import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras.models import Sequential
from keras.layers import Conv3D, MaxPooling3D, Flatten, Dense, Dropout
from sklearn.model_selection import train_test_split
from Load_Images import load_images_from_folder

# Establece CUDA_VISIBLE_DEVICES para usar solo la CPU
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"  

##################### PARÁMETROS ###############################

# Directorio con etiqueta 0 (Normal) y etiqueta 1 (Lesión de LCA)
data_dir_0 = "Dataset\\0"
data_dir_1 = "Dataset\\1"
# Tamaño de las imágenes
image_size = (128, 128)

##################### PREPARACIÓN DE DATOS #####################

# Cargar de imágenes y etiquetas para las clases 0 y 1
images_0, labels_0 = load_images_from_folder(data_dir_0, image_size, 0)
images_1, labels_1 = load_images_from_folder(data_dir_1, image_size, 1)

# Combinar imágenes y etiquetas
images = np.concatenate((images_0, images_1))
labels = np.concatenate((labels_0, labels_1))

# Dividir en conjuntos de entrenamiento y validación
images_train, images_valid, labels_train, labels_valid = train_test_split(images, labels, test_size=0.2, random_state=42)

##################### MODELO CNN 3D PERSONALIZADO ###############

# Crear el modelo
model = Sequential(
    [
        Conv3D(
            64, kernel_size=(3, 3, 3), activation="relu", input_shape=(9, 128, 128, 1)
        ),
        MaxPooling3D(pool_size=(2, 2, 2), padding="same"),
        Conv3D(128, kernel_size=(3, 3, 3), activation="relu"),
        MaxPooling3D(pool_size=(2, 2, 2)),
        Flatten(),
        Dense(256, activation="relu"),
        Dense(128, activation="relu"), 
        Dense(64, activation="relu"),
        Dropout(0.1), # 10% de dropout
        Dense(1, activation="sigmoid"),  # 1 neurona para clasificación binaria
    ]
)

# Compilación del modelo
model.compile(
    loss="binary_crossentropy",
    optimizer= keras.optimizers.Adam(learning_rate=1e-4), #Aprendizaje más lento
    metrics=[
        tf.keras.metrics.BinaryAccuracy(),
        tf.keras.metrics.AUC(),
    ],
)

# Guardar el mejor modelo según su binary accuracy
checkpoint = tf.keras.callbacks.ModelCheckpoint(
    filepath="./src/models/Best_CNN.h5",
    monitor= "binary_accuracy",
    mode="max",
    save_best_only=True,
    verbose=1,
)

# Ajustes en el entrenamiento
model.fit(
    images_train,
    labels_train,
    epochs= 30,
    batch_size= 32,
    validation_data=(images_valid, labels_valid),
    callbacks=[checkpoint],
)

# Muestra un resumen detallado del modelo
model.summary()
