import os
import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras.models import load_model
from sklearn.model_selection import train_test_split
from Load_Images import load_images_from_folder

##################### PARÁMETROS ###############################

# Directorio con carpetas de etiqueta 0 y etiqueta 1
data_dir_0 = "Dataset por etiqueta\\0-M"
data_dir_1 = "Dataset por etiqueta\\1-M"

# Tamaño de las imágenes
image_size = (128, 128)

##################### PREPARACIÓN DE DATOS #####################

# Cargar imágenes y etiquetas
images_0, labels_0 = load_images_from_folder(data_dir_0, image_size, 0)
images_1, labels_1 = load_images_from_folder(data_dir_1, image_size, 1)

# Combinar imágenes y etiquetas
images = np.concatenate((images_0, images_1))
labels = np.concatenate((labels_0, labels_1))

# Dividir en conjuntos de entrenamiento y validación
images_train, images_valid, labels_train, labels_valid = train_test_split(images, labels, test_size=0.2, random_state=42)

##################### MODELO PREENTRNADO #######################

# Cargar el modelo pre-entrenado
pretrained_model = load_model("src\\models\\Best_Fine_Tuned_Model.h5")

# Compilar el modelo con el optimizador, función de pérdida y métricas deseadas
pretrained_model.compile(
    optimizer=keras.optimizers.Adam(learning_rate=1e-5),  # Se puede ajustar la tasa de aprendizaje
    loss="binary_crossentropy",  # Ajusta la función de pérdida la tarea
    metrics=[tf.keras.metrics.BinaryAccuracy(), tf.keras.metrics.AUC()],
)

# Guardar el mejor modelo según su binary accuracy
checkpoint = tf.keras.callbacks.ModelCheckpoint(
    filepath="./src/models/Best_Fine_Tuned_Model.h5",
    monitor="binary_accuracy",
    mode="max",
    save_best_only=True,
    verbose=1,
)

# Entrenar el modelo con los datos de fine-tuning
pretrained_model.fit(
    images_train, 
    labels_train,  
    epochs= 20,  
    batch_size= 32,
    validation_data=(images_valid, labels_valid),  
    callbacks=[checkpoint],  
)

# Resumen del modelo
pretrained_model.summary()
