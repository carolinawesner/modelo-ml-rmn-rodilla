import os
import Edit_Images

################# PARA ARCHIVOS NUMPY #################

ruta_input_numpy = "./src/valid/sagittal" # Ruta donde se encuntras las imagenes en formato numpy
ruta_output_base = "./Dataset/imagenes"   # Ruta donde se crearán las carpetas con los casos

archivos_numpy = os.listdir(ruta_input_numpy)

for archivo in archivos_numpy:
    # Obtener el nombre del archivo sin extensión
    nombre_archivo = os.path.splitext(archivo)[0]

    # Ruta de salida para este archivo
    rutaOutput = os.path.join(ruta_output_base, nombre_archivo)

    # Cargar y procesar la imagen
    path = os.path.join(ruta_input_numpy, archivo)
    Edit_Images.numpy_to_jpeg(path, rutaOutput, 9, 0.5)

################# PARA ARCHIVOS JPEG #################

ruta_input_jpeg = "./src/valid/sagittal"
archivos_jpeg = os.listdir(ruta_input_jpeg)

for archivo in archivos_jpeg:
    # Obtener el nombre del archivo sin extensión
    nombre_archivo = os.path.splitext(archivo)[0]

    # Ruta de salida para este archivo
    rutaOutput = os.path.join(ruta_output_base, str(int(nombre_archivo)))
    os.makedirs(rutaOutput, exist_ok=True)

    # Cargar y procesar la imagen
    path = os.path.join(ruta_input_jpeg, archivo)
    Edit_Images.edit_jpeg(path, rutaOutput, 9, 0.4)
